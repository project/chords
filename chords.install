<?php // $Id$

/**
 * Implementation of hook_install().
 */
function chords_install() {
  drupal_install_schema('chords');
  
  //Populate the tables
  $chords = array(
    array('Maj','43','Lydian Mode'),
    array('min','34','Aeolian Mode'),
    array('7','433','Lydian b7'),
    array('m7','343','Aeolian Mode'),
    array('M7','434','Major Scale'),
    array('6','432','Lydian Mode'),
    array('m6','342','Asc. Melodic Minor'),
    array('dim','333','Lydian Diminished'),
    array('aug','44','+ (augmented)'),
    array('b5','42','Minor Pentatonic'),
    array('sus','52','Minor Pentatonic'),
    array('9','4334','Lydian b7'),
    array('m9','3434','Aeolian Mode'),
    array('add 9 Aeolian','214','Aeolian Mode'),
    array('add 9 Lydian','223','Lydian Mode'),
    array('Maj9','4343','Major Scale'),
    array('M7(b5)','425','Minor Pentatonic'),
    array('M9(#11)','43434','Minor Pentatonic'),
    array('M13','43437','Major Scale'),
    array('M7+','443','+ (augmented)'),
    array('M9+','4433','+ (augmented)'),
    array('M13(#11)','434343','Minor Pentatonic'),
    array('m7(b5)','334','Locrian #2'),
    array('m11','34343','Aeolian Mode'),
    array('m13','343434','Aeolian Mode'),
    array('m(M7)','344','Harmonic Minor'),
    array('m9(M7)','3443','Harmonic Minor'),
    array('7sus','523','Lydian b7'),
    array('9sus','5234','Lydian b7'),
    array('11','7343','Lydian b7'),
    array('13','43347','Lydian b7'),
    array('IV/V (Ex. F/G)','2554','Major Pentatonic'),
    array('7(b5)','424','Whole Tone'),
    array('9(b5)','4244','Whole Tone'),
    array('9(#11)','43344','Whole Tone'),
    array('13(#11)','433443','Whole Tone'),
    array('7(b9)','4333','Half-Whole Diminished'),
    array('11(b9)','43334','Half-Whole Diminished'),
    array('13(b9)','43338','Half-Whole Diminished'),
    array('7(#9)','4335','Blues Scale'),
    array('13(#9)','43356','Blues Scale'),
    array('7(b9#9)','43332','Half-Whole Diminished'),
    array('7(b9b5)','4243','Super Locrian'),
    array('7+','442','Whole Tone'),
    array('9+','4424','Whole Tone'),
    array('7+(#9)','4425','Super Locrian'),
    array('7+(b9)','4423','Super Locrian'),
    array('13(b9#11)','433353','Super Locrian')
  );
  
  foreach ($chords as $chord) {
    db_query("INSERT INTO {chords} (`name`, `pattern`, `scale`) VALUES ('%s', '%s', '%s');", $chord[0], $chord[1], $chord[2]);
  }
  
  variable_set('chords_keys', array("A", "Bb", "B", "C", "C#", "D", "Eb", "E", "F", "F#", "G", "G#"));
  variable_set('chords_instruments', array('Guitar' => array('EADGBE' => 'EADGBE', 'DADGAD' => 'DADGAD'), 'Banjo' => array('DGBD' => 'DGBD','DGCD' => 'DGCD', 'CGCD' => 'CGCD', 'CGDA' => 'CGDA'), 'Mandolin' => array('GDAE' => 'GDAE'), 'Ukulele' => array('CGEA' => 'CGEA'), 'Bass' => array('EADG' => 'EADG', 'BEADG' => 'BEADG', 'BEADGC' => 'BEADGC')));
  variable_set('chords_file_dir', 'chords');
  
  $success = file_create_path($chords_dir);

  if (!$success) {
    drupal_set_message(t("Could not find or create directory <tt>!chords_dir</tt>. The chords module will not function correctly until this directory is created.", array('!chords_dir' => $chords_dir)), 'error');
  }
}


function chords_uninstall() {
  drupal_uninstall_schema('chords');
  variable_del('chords_keys');
  variable_del('chords_instruments');
  variable_del('chords_file_dir');
}

/**
 * Implementation of hook_schema().
 */
function chords_schema() {
  $schema = array();
  $schema['chords'] = array(
    'description' => t("Stores information about chords."),
    'fields' => array(
      'cid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t("The chord's {chords}.cid.")
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'description' => t('The chord name.')
      ),
      'pattern' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'description' => t("The chord's pattern information.")
      ),
      'scale' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      )  
    ),
    'primary key' => array('cid')
  );
  
  return $schema;

}
