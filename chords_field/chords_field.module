<?php // $Id$

/**
 * @file
 * Defines a CCK Chord field. Many thanks to Andrew Childs for his example 
 * module at http://glomerate.wordpress.com/2009/11/09/a-compound-field-example-for-cck-2-and-drupal-6/
 */

/**
 * Implementation of hook_field_info().
 */
function chords_field_field_info() {
  return array(
    'chords_field' => array(
      'label' => t('Chord'),
      'description' => t('Stores a musical chord.'),
    ),
  );
}

/**
 * Implementation of hook_field_settings().
 */
function chords_field_field_settings($op, $field) {
  switch ($op) {
    
    case 'database columns':
      $columns['root']    = array('type' => 'varchar', 'length' => 2, 'not null' => FALSE, 'sortable' => TRUE, 'default' => '');
      $columns['chord'] = array('type' => 'varchar', 'length' => 15, 'not null' => FALSE, 'sortable' => FALSE, 'default' => '');
      return $columns;
  }
}

/**
 * Implementation of hook_content_is_empty().
 */
function chords_field_content_is_empty($item, $field) {
  if (empty($item['root']) && empty( $item['chord'])) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implementation of hook_theme().
 */
function chords_field_theme() {
  return array(
    'chords_field_formatter_default' => array(
      'arguments' => array('element' => NULL),
    ),
    'chords_field' => array(
      'arguments' => array('element' => NULL),
    ),
  );
}

/**
 * Implementation of hook_field_formatter_info().
 */
function chords_field_field_formatter_info() {
  return array(
    'default' => array(
      'label' => t('Default'),
      'field types' => array('chords_field'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
  );
}

/**
 * Theme function for 'default' example field formatter.
 *
 * $element['#item']: the sanitized $delta value for the item,
 * $element['#field_name']: the field name,
 * $element['#type_name']: the $node->type,
 * $element['#formatter']: the $formatter_name,
 * $element'#node']: the $node,
 * $element['#delta']: the delta of this item, like '0',
 *
 */
function theme_chords_field_formatter_default($element) {
  $output = '';
  $output .= '<div class="chords-field-root">' . $element['#item']['root'] . '</div>';
  $output .= '<div class="chords-field-chord">' . $element['#item']['chord'] . '</div>';
  return $output;
}

/**
 * FAPI theme for an individual element.
 */
function theme_chords_field($element) {
  // If you want to add CSS (or JS) for this form, here's where you would:
  // drupal_add_css(drupal_get_path('module', 'chords_field') .'/chords_field.css');
  $output = '';
  $output .= '<div class="chords-field">';
  $output .= theme('select', $element['root']);
  $output .= theme('select', $element['chord']);
  $output .= '</div>';
  return $output;
}

/**
 * Implementation of FAPI hook_elements().
 *
 * Any FAPI callbacks needed for individual widgets can be declared here,
 * and the element will be passed to those callbacks for processing.
 *
 * Drupal will automatically theme the element using a theme with
 * the same name as the hook_elements key.
 *
 * Autocomplete_path is not used by text_widget but other widgets can use it
 * (see nodereference and userreference).
 */
function chords_field_elements() {
  $elements = array(
    'chords_field' => array(
      '#input' => TRUE,
      '#process' => array('chords_field_process'),
    ),
  );
  return $elements;
}

/**
 * Implementation of hook_widget_info().
 *
 * Here we indicate that the content module will handle
 * the default value and multiple values for these widgets.
 *
 * Callbacks can be omitted if default handing is used.
 * They're included here just so this module can be used
 * as an example for custom modules that might do things
 * differently.
 */
function chords_field_widget_info() {
  return array(
    'chords_field' => array(
      'label' => 'Chord Field',
      'field types' => array('chords_field'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
  );
}

/**
 * Implementation of hook_widget().
 *
 * Attach a single form element to the form. It will be built out and
 * validated in the callback(s) listed in hook_elements. We build it
 * out in the callbacks rather than here in hook_widget so it can be
 * plugged into any module that can provide it with valid
 * $field information.
 *
 * Content module will set the weight, field name and delta values
 * for each form element. This is a change from earlier CCK versions
 * where the widget managed its own multiple values.
 *
 * If there are multiple values for this field, the content module will
 * call this function as many times as needed.
 *
 * @param $form
 *   the entire form array, $form['#node'] holds node information
 * @param $form_state
 *   the form_state, $form_state['values'][$field['field_name']]
 *   holds the field's form values.
 * @param $field
 *   the field array
 * @param $items
 *   array of default values for this field
 * @param $delta
 *   the order of this item in the array of subelements (0, 1, 2, etc)
 *
 * @return
 *   the form item for a single element for this field
 */
function chords_field_widget(&$form, &$form_state, $field, $items, $delta = 0) {
  $element = array(
    '#type' => $field['widget']['type'],
    '#default_value' => isset($items[$delta]) ? $items[$delta] : '',
    '#title' => $field['widget']['label'],
    '#field' => $field,
  );
  return $element;
}

/**
 * Process an individual element.
 *
 * Build the form element. When creating a form using FAPI #process,
 * note that $element['#value'] is already set.
 *
 * The $fields array is in $form['#field_info'][$element['#field_name']].
 */
function chords_field_process($element, $edit, $form_state, $form) {
  $root_options = array('-' => '');
  $roots = variable_get('chords_keys', array("A", "Bb", "B", "C", "C#", "D", "Eb", "E", "F", "F#", "G", "G#"));
  foreach ($roots as $option) {
    $root_options[$option] = $option;
  }
  
  $chord_options = array('-' => '');
  $chords = chords_get_chords();
  foreach ($chords as $chord) {
    $chord_options[$chord['name']] = $chord['name'];
  }
  dpm($items[$delta]);
  $element['root'] = array(
    '#type' => 'select',
    '#options' => $root_options,
    '#default_value' => isset($element['#value']['root']) ? $element['#value']['root'] : '',
    '#title' => t('Root')
  );
  
  $element['chord'] = array(
    '#type' => 'select',
    '#options' => $chord_options,
    '#default_value' => isset($element['#value']['chord']) ? $element['#value']['chord'] : '',
    '#title' => t('Chord'),
  );
  
  
  return $element;
}
